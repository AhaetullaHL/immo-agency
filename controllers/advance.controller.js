const Advance = require('../models').Advance;

exports.advance_list = (req, res, next) => {
  Advance.findAll({
    order: [
      ['label', 'ASC']
    ],
    attributes: [
      'id',
      'label',
    ],
  })
    .then( advances => res.status(200).json(advances))
    .catch(err => console.log(err))
}

exports.advance_add = (req, res, next) => {
  const advance = req.body;
  Advance.create(advance)
    .then( advanceCreated => res.status(201).json(advanceCreated))
    .catch(error => {
      if(error.label === "SequelizeUniqueConstraintError"){
        res.status(403).json({'error': `Duplicate entry. Impossible to add`});
      } else {
        res.status(400).json({'error': `Can't add, bad fields. Check the documentation`});
      }
    })
}

exports.advance_detail = (req, res, next) =>{
  const id = parseInt(req.params.id);
  Advance.findByPk(id)
  .then(advance => res.status(200).json(advance))
  .catch(err => console.log(err))
}

exports.advance_edit = (req, res, next) =>{
  const id = parseInt(req.params.id);
  const advance = req.body;
  Advance.update(advance, {
    where: {
      id: id
    }
  })
  .then(() => {
    res.status(200).json({message: 'Advance updated'})
  })
  .catch(err => console.log(err))
}

exports.advance_delete = (req, res, next) =>{
  const id = parseInt(req.params.id);
  Advance.destroy({
    where: {
      id: id
    }
  })
  .then(advance => res.status(200).json({message: 'Advance deleted'}))
  .catch(err => console.log(err))
}

