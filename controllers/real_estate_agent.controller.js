const Agent = require('../models').Real_Estate_Agent;
const Ad = require('../models').Ad;
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

exports.agent_list = (req, res, next) => {
  Agent.findAll({
    order: [
      ['id', 'ASC']
    ],
    attributes: [
      'id',
      'name',
      'age',
      'email',
      'phone',
      'role',
    ],
  })
    .then( agents => res.status(200).json(agents))
    .catch(err => console.log(err))
}

exports.agent_add = (req, res, next) => {
  bcrypt.hash(req.body.password, 10, (err, hash) => {
    if (err)
      throw err;
    let agent = req.body;
    req.body.password = hash;
    Agent.create(agent)
      .then( agentCreated => res.status(201).json(agentCreated))
      .catch(error => {
        if(
          error.name === "SequelizeUniqueConstraintError" ||
          error.email === "SequelizeUniqueConstraintError"
        ){
          res.status(403).json({'error': `Duplicate entry. Impossible to add`});
        } else {
          res.status(400).json({'error': `Can't add, bad fields. Check the documentation`});
        }
      })
  })
}


exports.agent_login = (req, res, next) => {
  Agent.findOne({
    where: {
      email: req.body.email
    }
  })
  .then(agent => {
    if(agent) {
      bcrypt.compare(req.body.password, agent.password, (err, result) => {
        if (err) return res.status(500).json(err)
        else {
          if (result) {
            const token = jwt.sign({
              email: agent.email,
              name: agent.name,
              role: agent.role
            }, process.env["SECRET_PHRASE"], {expiresIn: '240d'})
            res.status(200).json({
              token: token
            })
          }
          else return res.json({message: 'Fail'})
        }
      })
    }else {
      res.status(404).json({message: 'Bad login / password'})
    }
  })
  .catch(err => {
    res.status(500).json(err)
  })
}

exports.agent_detail = (req, res, next) =>{
  const id = parseInt(req.params.id);
  Agent.findByPk(id)
    .then(agent => res.status(200).json(agent))
    .catch(err => console.log(err))
}

exports.agent_edit = (req, res, next) =>{
  const id = parseInt(req.params.id);
  const agent = req.body;
  Agent.update(agent, {
    where: {
      id: id
    }
  })
    .then(() => {
      res.status(200).json({message: 'Agent updated'})
    })
    .catch(err => console.log(err))
}

exports.agent_delete = (req, res, next) =>{
  const id = parseInt(req.params.id);
  Agent.destroy({
    where: {
      id: id
    }
  })
    .then(agent => res.status(200).json({message: 'Agent deleted'}))
    .catch(err => console.log(err))
}

