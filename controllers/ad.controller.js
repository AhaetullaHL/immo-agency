const Ad = require('../models').Ad;
const Advance = require('../models').Advance;

exports.ad_list = (req, res, next) => {
  Ad.findAll({
    order: [
      ['updatedAt', 'DESC']
    ],
    attributes: [
      'id',
      'title',
      'price',
      'area',
      'rooms',
      'description',
      'picture',
      'real_estate_agent',
      'props_type'
    ],
    include : [
      {
        model: Advance,
        attributes: ['id', 'label'],
        through: { attributes: [] }
      }
    ]
  })
  .then( ads => res.status(200).json(ads))
  .catch(err => console.log(err))
}

exports.ad_add = (req, res, next) => {
  const ad = req.body;
  Ad.create(ad)
  .then( adCreated => res.status(201).json(adCreated))
  .catch(error => {
    if(error.title === "SequelizeUniqueConstraintError"){
      res.status(403).json({'error': `Duplicate entry. Impossible to add`});
    } else {
      res.status(400).json({'error': `Can't add, bad fields. Check the documentation`});
    }
  })
}

exports.ad_detail = (req, res, next) =>{
  const id = parseInt(req.params.id);
  Ad.findByPk(id)
  .then(ad => res.status(200).json(ad))
  .catch(err => console.log(err))
}

exports.ad_edit = (req, res, next) =>{
  const id = parseInt(req.params.id);
  const ad = req.body;
  Ad.update(ad, {
    where: {
      id: id
    }
  })
  .then(() => {
    res.status(200).json({message: 'Ad updated'})
  })
  .catch(err => console.log(err))
}

exports.ad_delete = (req, res, next) =>{
  const id = parseInt(req.params.id);
  Ad.destroy({
    where: {
      id: id
    }
  })
  .then(ad => res.status(200).json({message: 'Ad deleted'}))
  .catch(err => console.log(err))
}

