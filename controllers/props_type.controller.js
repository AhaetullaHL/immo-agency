const Type = require('../models').Props_Type;

exports.props_type_list = (req, res, next) => {
  Type.findAll({
    order: [
      ['label', 'ASC']
    ],
    attributes: [
      'id',
      'label',
    ],
  })
    .then( props_types => res.status(200).json(props_types))
    .catch(err => console.log(err))
}

exports.props_type_add = (req, res, next) => {
  const props_type = req.body;
  Type.create(props_type)
    .then( props_typeCreated => res.status(201).json(props_typeCreated))
    .catch(error => {
      if(error.label === "SequelizeUniqueConstraintError"){
        res.status(403).json({'error': `Duplicate entry. Impossible to add`});
      } else {
        res.status(400).json({'error': `Can't add, bad fields. Check the documentation`});
      }
    })
}

exports.props_type_detail = (req, res, next) =>{
  const id = parseInt(req.params.id);
  Type.findByPk(id)
    .then(props_type => res.status(200).json(props_type))
    .catch(err => console.log(err))
}

exports.props_type_edit = (req, res, next) =>{
  const id = parseInt(req.params.id);
  const props_type = req.body;
  Type.update(props_type, {
    where: {
      id: id
    }
  })
    .then(() => {
      res.status(200).json({message: 'Props_Type updated'})
    })
    .catch(err => console.log(err))
}

exports.props_type_delete = (req, res, next) =>{
  const id = parseInt(req.params.id);
  Type.destroy({
    where: {
      id: id
    }
  })
    .then(props_type => res.status(200).json({message: 'Props_Type deleted'}))
    .catch(err => console.log(err))
}

