const KeyWord = require('../models').KeyWord;

exports.keyword_list = (req, res, next) => {
  KeyWord.findAll({
    order: [
      ['label', 'ASC']
    ],
    attributes: [
      'id',
      'label',
    ],
  })
    .then( keywords => res.status(200).json(keywords))
    .catch(err => console.log(err))
}

exports.keyword_add = (req, res, next) => {
  const keyword = req.body;
  KeyWord.create(keyword)
    .then( keywordCreated => res.status(201).json(keywordCreated))
    .catch(error => {
      if(error.label === "SequelizeUniqueConstraintError"){
        res.status(403).json({'error': `Duplicate entry. Impossible to add`});
      } else {
        res.status(400).json({'error': `Can't add, bad fields. Check the documentation`});
      }
    })
}

exports.keyword_detail = (req, res, next) =>{
  const id = parseInt(req.params.id);
  KeyWord.findByPk(id)
    .then(keyword => res.status(200).json(keyword))
    .catch(err => console.log(err))
}

exports.keyword_edit = (req, res, next) =>{
  const id = parseInt(req.params.id);
  const keyword = req.body;
  KeyWord.update(keyword, {
    where: {
      id: id
    }
  })
    .then(() => {
      res.status(200).json({message: 'Keyword updated'})
    })
    .catch(err => console.log(err))
}

exports.keyword_delete = (req, res, next) =>{
  const id = parseInt(req.params.id);
  KeyWord.destroy({
    where: {
      id: id
    }
  })
    .then(keyword => res.status(200).json({message: 'Keyword deleted'}))
    .catch(err => console.log(err))
}

