const express = require('express');
const router = express.Router();
const props_type_controller = require('../controllers/props_type.controller');
const auth = require('../middleware/auth');

router.get('/', props_type_controller.props_type_list);
router.get('/:id', props_type_controller.props_type_detail);
router.post('/', auth(), props_type_controller.props_type_add);
router.put('/:id', auth(), props_type_controller.props_type_edit);
router.delete('/:id', auth(), props_type_controller.props_type_delete);

module.exports = router;
