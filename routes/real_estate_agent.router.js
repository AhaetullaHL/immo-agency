const express = require('express');
const router = express.Router();
const real_estate_agent_controller = require('../controllers/real_estate_agent.controller');
const admin = require('../middleware/admin');

router.get('/', real_estate_agent_controller.agent_list);
router.get('/:id', real_estate_agent_controller.agent_detail);
router.post('/', admin(), real_estate_agent_controller.agent_add);
router.post('/login', real_estate_agent_controller.agent_login);
router.put('/:id', admin(), real_estate_agent_controller.agent_edit);
router.delete('/:id', admin(), real_estate_agent_controller.agent_delete);

module.exports = router;
