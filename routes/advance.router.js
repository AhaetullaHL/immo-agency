const express = require('express');
const router = express.Router();
const advance_controller = require('../controllers/advance.controller');
const auth = require('../middleware/auth');

router.get('/', advance_controller.advance_list);
router.get('/:id', advance_controller.advance_detail);
router.post('/', auth(), advance_controller.advance_add);
router.put('/:id', auth(), advance_controller.advance_edit);
router.delete('/:id', auth(), advance_controller.advance_delete);

module.exports = router;
