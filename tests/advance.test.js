const request = require('supertest');
const app = require('../app');
require('dotenv').config();

describe('Advances EndPoints', () => {
    const token = process.env['TOKEN'];

    describe('GET /advances', () => {
        //test OK
        it('Should fetch all advances', async () => {
            const res = await request(app)
              .get('/advances')
            expect(res.statusCode).toEqual(200);
        })
    })

    describe('POST /advances', () => {
        //test OK
        it('Should create a new advance', async () => {
            const res = await request(app)
              .post('/advances')
              .set('Authorization', `Bearer ${token}`)
              .send({"label": "Excellent"})
            expect(res.statusCode).toEqual(201)
            expect(res.body).toHaveProperty('label')
        })
        // test OK
        it('Throw error if POST error doesn\'t have correct field', async () => {
            const res = await request(app)
              .post('/advances')
              .set('Authorization', `Bearer ${token}`)
              .send({"fakename": "htfdgr"})
            expect(res.statusCode).toEqual(400)
        })

        //TODO: check error test validation
        it('Throw error if duplicate advance', async () => {
            const res = await request(app)
              .post('/advances')
              .set('Authorization', `Bearer ${token}`)
              .send({"label": "Super"})
            expect(res.statusCode).toEqual(403)
        })
    })
})



