'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Real_Estate_Agent extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Real_Estate_Agent.hasMany(models.Ad);
    }
  };
  Real_Estate_Agent.init({
    name: {
      type: DataTypes.STRING,
      unique: true,
    },
    age: DataTypes.INTEGER,
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      validate: {
        //check if email format
        isEmail: true
      },
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false
    },
    phone: {
      type: DataTypes.INTEGER,
      unique: true
    },
    role: {
      type: DataTypes.STRING,
      defaultValue: "agent",
      validate: {
        //check if is only admin or user role values -> permissions
        isIn: [['admin', 'agent']]
      },
    },
  }, {
    sequelize,
    modelName: 'Real_Estate_Agent',
  });
  return Real_Estate_Agent;
};
