'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class AdAdvance extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      AdAdvance.belongsTo(models.Ad);
      AdAdvance.belongsTo(models.Advance);
    }
  };
  AdAdvance.init({
  }, {
    sequelize,
    modelName: 'AdAdvance',
  });
  return AdAdvance;
};
