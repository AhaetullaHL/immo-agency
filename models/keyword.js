'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class KeyWord extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      KeyWord.belongsToMany(models.Ad, {through: models.AdKeyword});
      KeyWord.hasMany(models.AdKeyword);
    }
  };
  KeyWord.init({
    label: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false
    }
  }, {
    sequelize,
    modelName: 'KeyWord',
  });
  return KeyWord;
};
