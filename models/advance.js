'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Advance extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Advance.belongsToMany(models.Ad, {through: models.AdAdvance});
      Advance.hasMany(models.AdAdvance);
    }
  };
  Advance.init({
    label: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false
    },
  }, {
    sequelize,
    modelName: 'Advance',
  });
  return Advance;
};
