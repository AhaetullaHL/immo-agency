'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Ad extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Ad.belongsTo(models.Real_Estate_Agent);
      Ad.belongsTo(models.Props_Type);
      Ad.belongsToMany(models.KeyWord, {through: models.AdKeyword});
      Ad.hasMany(models.AdKeyword);
      Ad.belongsToMany(models.Advance, {through: models.AdAdvance});
      Ad.hasMany(models.AdAdvance);
    }
  };
  Ad.init({
    title: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    price: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    area: {
      type: DataTypes.STRING,
      allowNull: false
    },
    rooms: {
      type: DataTypes.INTEGER,
      defaultValue: 1,
      min: 0
    },
    description: DataTypes.TEXT,
    picture: {
      type: DataTypes.STRING,
      defaultValue: "https://picsum.photos/300/200"
    },
  }, {
    sequelize,
    modelName: 'Ad',
  });
  return Ad;
};
