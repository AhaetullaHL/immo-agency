'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Ads', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      title: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true
      },
      price: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      area: {
        type: Sequelize.STRING,
        allowNull: false
      },
      rooms: {
        type: Sequelize.INTEGER,
        defaultValue: 1,
        min: 0
      },
      description: {
        type: Sequelize.TEXT
      },
      picture: {
        type: Sequelize.STRING,
        defaultValue: "https://picsum.photos/300/200"
      },
      real_estate_agent: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'Real_Estate_Agents',
          key: 'id'
        }
      },
      props_type: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'Props_Types',
          key: 'id'
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Ads');
  }
};
