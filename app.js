require('dotenv').config();
const morgan = require('morgan');
const path = require('path');
const fs = require('fs');

const express = require('express');
const cors = require('cors');
const app = express();

//Import routers
const adRouter = require('./routes/ad.router');
const advanceRouter = require('./routes/advance.router');
const keywordRouter = require('./routes/keyword.router');
const typeRouter = require('./routes/props_type.router');
const agentRouter = require('./routes/real_estate_agent.router');

//logs
const accessLogStream = fs.createWriteStream(path.join(__dirname, 'access.log'), {flags: 'a' });
app.use(morgan('combined', {stream: accessLogStream}));

const port = 3000;

app.use(express.json());
app.use(express.urlencoded({ extended: false }))

// Protect from CORS error
app.use(cors());

app.use('/ads', adRouter);
app.use('/advances', advanceRouter);
app.use('/keywords', keywordRouter);
app.use('/types', typeRouter);
app.use('/agents', agentRouter);

app.listen( port, () => {});

module.exports = app;
