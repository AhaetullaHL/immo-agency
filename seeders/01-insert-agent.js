'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('Real_Estate_Agents', [
      {
        name: "admin",
        age: 25,
        email: "admin@admin.com",
        password: "$2b$10$JauFdA7GC/czfTLs/XS7NeyABMec5CAI3dDEmkcXsH8YpZZU3LCoK",
        phone: 1234567890,
        role: "admin",
        createdAt: new Date(),
        updatedAt: new Date()
      },
    ])
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Real_Estate_Agents', null, {})
  }
};
